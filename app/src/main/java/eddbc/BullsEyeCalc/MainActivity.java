package eddbc.BullsEyeCalc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

	public final String TAG = "BRA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

	    Button calc_button = (Button) findViewById(R.id.calc_button);

	    calc_button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
			    Log.i(TAG, "Hello World");
			    doCalc();
		    }
	    });
    }

	private void doCalc() {
		EditText b1Field = (EditText) findViewById(R.id.b1_field);
		EditText r1Field = (EditText) findViewById(R.id.r1_field);
		EditText b2Field = (EditText) findViewById(R.id.b2_field);
		EditText r2Field = (EditText) findViewById(R.id.r2_field);

		Double b1 = Double.parseDouble(b1Field.getText().toString());
		Double r1 = Double.parseDouble(r1Field.getText().toString());
		Double b2 = Double.parseDouble(b2Field.getText().toString());
		Double r2 = Double.parseDouble(r2Field.getText().toString());

		BullsEyeSolver bs = new BullsEyeSolver(b1, r1, b2, r2);

		String testStr = "Delta X: "+bs.getX()+", Delta Y: "+bs.getY()+", Range: "+bs.getRange()+", Bearing: "+bs.getBearing();
		String resStr = "Range: "+bs.getRange()+", Bearing: "+bs.getBearing();

		TextView resultLabel = (TextView) findViewById(R.id.result_field);
		resultLabel.setText(resStr);

		Log.i(TAG, testStr);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
