package eddbc.BullsEyeCalc;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by edward on 16/10/2014.
 */
public class BullsEyeSolver {

	Double x;
	Double y;
	Double range;
	Double bearing;

	@SuppressWarnings("SuspiciousNameCombination")
	public BullsEyeSolver(Double t1Bearing, Double t1Range, Double t2Bearing, Double t2Range) {

		Double x1 = -1 * t1Range * Math.sin(Math.toRadians(t1Bearing));
		Double y1 = -1 * t1Range * Math.cos(Math.toRadians(t1Bearing));
		Double x2 = t2Range * Math.sin(Math.toRadians(t2Bearing));
		Double y2 = t2Range * Math.cos(Math.toRadians(t2Bearing));

		x = x1 + x2;
		y = y1 + y2;

		range = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

		bearing = calculateCompassBearing(Math.toDegrees(Math.atan2(x,y)));
	}

	public double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public Double calculateCompassBearing(Double bearing) {

		if (bearing >= 360) {
			return calculateCompassBearing(bearing - 360);
		} else if (bearing < 0) {
			return calculateCompassBearing(bearing + 360);
		} else {
			return bearing;
		}
	}
	public Double getX() {
		return x;
	}

	public Double getY() {
		return y;
	}

	public Double getRange() {
		return round(range, 1);
	}

	public Double getBearing() {
		return round(bearing, 0);
	}
}
