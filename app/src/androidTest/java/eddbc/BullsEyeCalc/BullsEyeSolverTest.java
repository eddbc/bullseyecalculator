package eddbc.BullsEyeCalc;

import junit.framework.TestCase;

public class BullsEyeSolverTest extends TestCase {

	public void testUnitCircle() throws Exception {

		BullsEyeSolver bs;

		// Test 0 and 90 on a unit circle
		bs = new BullsEyeSolver(0d, 1d, 90d, 1d);
		assertEquals(1, bs.getX(), 0.01);
		assertEquals(-1, bs.getY(), 0.01);
		assertEquals(135, bs.bearing, 0.1);
		assertEquals(Math.sqrt(2), bs.range, 0.01);

		// Test 0 and 180 on a unit circle
		bs = new BullsEyeSolver(0d, 1d, 180d, 1d);
		assertEquals(0, bs.getX(), 0.1);
		assertEquals(-2, bs.getY(), 0.1);
		assertEquals(180, bs.bearing, 0.1);
		assertEquals(2, bs.range, 0.01);
	}
}