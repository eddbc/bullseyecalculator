Bullseye Calculator
=========

Quick description of bullseye locations for non-flight-sim-nerds :

There is a central reference point called a bullseye, like [this](http://www.185th.co.uk/squad_info/training/n&b_01.gif).
You are given your bearing and range from the bullseye, and the same for a target.
But actually knowing where that means the target is from you requires some mental maths on your part, and even then won't be completely accurate.

This app does the calculations to give you a more precise idea of where you should be flying with a bearing and range to target.

![screenshot](http://i.imgur.com/2c8J26B.png)
